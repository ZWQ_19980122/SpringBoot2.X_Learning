package cn.xxxq.springboot_springsecurity.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     *  自定义配置
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/css/**","/js/**","/index").permitAll()//都可以访问
                .antMatchers("/users/**").hasRole("ADMIN")//需要相应的角色才能访问
                .and()
                .formLogin()//基于Form表单登录验证
                .loginPage("/login").failureUrl("/login-error");//自定义登录页面
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        //认证信息存储于内存中
//        auth.inMemoryAuthentication().withUser("zwq").password("xxxq").roles("ADMIN");
        auth.inMemoryAuthentication().passwordEncoder(new MyPasswordEncoder()).withUser("zwq").password("xxxq").roles("ADMIN");
    }
}