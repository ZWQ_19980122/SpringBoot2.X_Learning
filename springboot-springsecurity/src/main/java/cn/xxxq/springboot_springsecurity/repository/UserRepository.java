package cn.xxxq.springboot_springsecurity.repository;

import cn.xxxq.springboot_springsecurity.pojo.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Long> {
}