# SpringBoot2.X_Learning

#### 介绍
学习SpringBoot2.X的源码库

#### springboot-initialize：
    使用spring脚手架创建springboot项目，
    详情请看springboot-initialize\readme.md
#### springboot-thymeleaf：
    SpringBoot整合thymeleaf,详情请看springboot-thymeleaf\readme.md 
#### springboot-jpa
    Springboot整合spring data jpa，详情请看springboot-jpa\readme.md 