package cn.xxxq.springboot_jpa.repository;

import cn.xxxq.springboot_jpa.pojo.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Long> {
}