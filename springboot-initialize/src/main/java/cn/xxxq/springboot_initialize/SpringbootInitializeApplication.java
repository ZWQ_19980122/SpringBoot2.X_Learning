package cn.xxxq.springboot_initialize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootInitializeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootInitializeApplication.class, args);
    }

}