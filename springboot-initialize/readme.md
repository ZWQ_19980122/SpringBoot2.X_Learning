# springboot-initialize
该模块是使用Spring脚手架创建的SpringBoot项目。

#### 引入的依赖
```xml
<!-- 
    每个SpringBoot项目都需要依赖该父工程 
    该父工程管理了众多jar包的版本，解决了依赖冲突问题
-->
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.2.2.RELEASE</version>
    <relativePath/> <!-- lookup parent from repository -->
</parent>
<dependencies>
    <!--Web层的相关依赖——SpringMVC-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <!--SpringBoot测试jar包-->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
        <exclusions>
            <exclusion>
                <groupId>org.junit.vintage</groupId>
                <artifactId>junit-vintage-engine</artifactId>
            </exclusion>
        </exclusions>
    </dependency>
</dependencies>
```

SpringBoot只要把项目打包成jar包，也能部署到服务器上，供浏览器用户访问。
每个SpringBoot项目都会有一个启动类，类上添加@SpringBootApplication注解，
类中编写main方法，用来启动SpringBoot项目。

@RestController = @Controller + @ResponseBody
如果某个Controller类上加入@RestController注解，那么该类的所有处理响应的方法只会返回JSON数据。

该项目主要在浏览器打印"Hello SpringBoot!"字符串内容。
访问地址：http://localhost:8080/hello