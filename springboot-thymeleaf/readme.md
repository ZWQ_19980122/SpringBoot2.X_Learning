# springboot-thymeleaf

#### springboot整合thymeleaf

需要引入thymeleaf启动器starter,版本由springboot管理了

```xml
<dependency>    
	<groupId>org.springframework.boot</groupId>    
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
```

引入Lombok插件，简化JavaBean代码

```xml
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <optional>true</optional>
</dependency>
```

引入Web启动器，支持SpringMvc相关Web功能

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>
```

数据持久层框架使用mybatis，所以也要引入mybatis启动器及mybatis框架依赖

```xml
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis</artifactId>
    <version>3.5.3</version>
</dependency>
<dependency>
    <groupId>org.mybatis.spring.boot</groupId>
    <artifactId>mybatis-spring-boot-starter</artifactId>
    <version>2.1.1</version>
</dependency>
```

还要引入Java程序与数据库之间连接的桥梁——MySQL驱动包

```xml
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <!--SpringBoot2.X版本管理的是Mysql8.X版本，我们改成5.X版本的-->
    <version>5.1.47</version>
</dependency>
```

由于只是对用户表进行操作，也就是简单的单表操作，引入通用mapper插件帮助我们开发

```xml
<dependency>
    <groupId>tk.mybatis</groupId>
    <artifactId>mapper-spring-boot-starter</artifactId>
    <version>2.1.5</version>
</dependency>
```

**什么是通用mapper？**

就是一个封装了一些mybatis框架功能的组件。以前没有使用通用mapper的时候，我们还需要编写Mapper.xml映射文件，将Mapper接口与之绑定，现在使用通用mapper之后，只要是单表操作，我们就不用编写mapper.xml映射文件了。直接编写Mapper接口,然后继承`tk.mybatis.mapper.common.Mapper`(通用mapper中的接口)即可。

```java
package cn.xxxq.springboot_thymeleaf.mapper;

import cn.xxxq.springboot_thymeleaf.pojo.User;
import tk.mybatis.mapper.common.Mapper;

@Repository
public interface UserMapper extends Mapper<User> {
}
```



### thymeleaf简介

### springboot结合thymeleaf实战

#### API设计

**API就是一系列的接口集合。可以提供给别人访问。这里定义的就是对用户操作的一些接口**

GET：/users：返回用于展现用户列表的list.html页面

GET：/users/{id}：返回用于展现用户的view.html页面

GET：/users/form：返回用于新增或者修改用户的form.html页面

POST：/users：新增或者修改用户，成功后重定向到list.html页面

GET：/users/delete/{id}：根据id删除相应的用户数据，成功后重定向到list.html页面

GET：/users/modify/{id}：根据id获取相应的用户数据，并返回form.html页面用来执行修改

#### 后台编码

##### 数据库表：users

```sql
CREATE TABLE `users` (
  `uid` int(10) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `uname` varchar(255) DEFAULT NULL COMMENT '用户姓名',
  `pwd` varchar(255) DEFAULT NULL COMMENT '用户密码',
  `sex` int(1) DEFAULT NULL COMMENT '用户性别，1：男，0：女',
  `age` int(3) DEFAULT NULL COMMENT '用户年龄',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
```

##### 实体：User

```java
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    private long uid; // 用户的唯一标识
    private String uname;
    private String pwd;
    private int age;
    private int sex;

}
```

控制器：UserController

业务层：UserService

Mapper：UserMapper



前端页面：

list.html：显示全部用户信息

form：修改或新增用户信息

view：用户个人信息

```html
<!DOCTYPE html>
<html lang="en" xmlns="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>用户列表</title>
</head>
<body>

</body>
</html>
```

如果一个HTML页面使用thymeleaf，需要在html标签中引入thymeleaf的命名空间。

**xmlns="http://www.thymeleaf.org"**