package cn.xxxq.springboot_thymeleaf.service;

import cn.xxxq.springboot_thymeleaf.mapper.UserMapper;
import cn.xxxq.springboot_thymeleaf.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     *  查询全部用户
     * @return
     */
    public List<User> selectUsers(){
        return userMapper.selectAll();
    }

    /**
     *  根据用户id查询用户
     * @param uid
     * @return
     */
    public User selectUserByUid(Long uid){
        return userMapper.selectByPrimaryKey(uid);
    }

    /**
     *  保存用户
     * @param user
     */
    public void insert(User user){
        userMapper.insertSelective(user);
    }

    /**
     * 根据用户id修改用户
     * @param user
     */
    public void update(User user){
        userMapper.updateByPrimaryKeySelective(user);
    }

    /**
     *  根据用户id删除用户
     * @param uid
     */
    public void delete(Long uid){
        userMapper.deleteByPrimaryKey(uid);
    }
}