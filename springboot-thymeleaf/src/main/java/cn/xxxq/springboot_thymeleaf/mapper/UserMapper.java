package cn.xxxq.springboot_thymeleaf.mapper;

import cn.xxxq.springboot_thymeleaf.pojo.User;
import tk.mybatis.mapper.common.Mapper;

//@Repository
public interface UserMapper extends Mapper<User> {
}
