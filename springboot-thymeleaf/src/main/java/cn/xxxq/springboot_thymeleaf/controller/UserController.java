package cn.xxxq.springboot_thymeleaf.controller;

import cn.xxxq.springboot_thymeleaf.pojo.User;
import cn.xxxq.springboot_thymeleaf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    //GET：/users：返回用于展现用户列表的list.html页面
    @GetMapping
    public ModelAndView users(ModelAndView modelAndView){
        List<User> users = userService.selectUsers();
        modelAndView.addObject("users",users);
        modelAndView.setViewName("/user/list");
        return modelAndView;
    }

    //GET：/users/{uid}：返回用于展现用户的view.html页面
    @GetMapping("{uid}")
    public ModelAndView view(@PathVariable("uid") Long uid,ModelAndView modelAndView){
        User user = userService.selectUserByUid(uid);
        modelAndView.addObject("user",user);
        modelAndView.setViewName("user/view");
        return modelAndView;
    }

    //GET：/users/form：返回用于新增或者修改用户的form.html页面
    @GetMapping("/form")
    public ModelAndView forms(User user,ModelAndView modelAndView){
        //user ：有我就接收，没有我就为null
        modelAndView.addObject("user",user);
        modelAndView.setViewName("user/form");
        return modelAndView;
    }

    //POST：/users：新增或者修改用户，成功后重定向到list.html页面
    @PostMapping
    public ModelAndView saveOrUpdate(User user){
        /*
            新增和修改操作在同一个接口中，那怎么判断是新增还是修改？
                新增一般用户id为null，而修改用户id不为null
         */
        if(user.getUid()!=null){
            //修改用户
            userService.update(user);
        }else {
            //新增操作
            userService.insert(user);
        }
        return new ModelAndView("redirect:/users");
    }

    //GET：/users/delete/{id}：根据id删除相应的用户数据，成功后重定向到list.html页面
    @GetMapping("/delete/{uid}")
    public ModelAndView delete(@PathVariable("uid") Long uid){
        userService.delete(uid);
        return new ModelAndView("redirect:/users");
    }

    //GET：/users/modify/{id}：根据id获取相应的用户数据，并返回form.html页面用来执行修改
    @GetMapping("/modify/{uid}")
    public ModelAndView modify(@PathVariable("uid") Long uid,ModelAndView modelAndView){
        User user = userService.selectUserByUid(uid);
        modelAndView.addObject("user",user);
        modelAndView.setViewName("/user/form");
        return modelAndView;
    }
}