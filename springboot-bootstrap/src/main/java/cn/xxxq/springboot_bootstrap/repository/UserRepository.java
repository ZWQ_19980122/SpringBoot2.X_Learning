package cn.xxxq.springboot_bootstrap.repository;

import cn.xxxq.springboot_bootstrap.pojo.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Long> {
}